import XCTest
@testable import Utilities

final class UtilitiesTests: XCTestCase {

    func testGetSerial() {
        let serial = try? getSerial()
        XCTAssertNotNil(serial, "Could not get serial, but didn't throw either.")
    }

    func testGetMAC() {
        let MAC = getMAC()
        XCTAssertNotNil(MAC, "Could not get MAC")
    }

    static var allTests = [
        ("testGetSerial", testGetSerial,
         "testGetMAC", testGetMAC)
    ]
}
