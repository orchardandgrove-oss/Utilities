//
//  NSTaskWrapper.swift
//
//  Created by Joel Rennich on 3/29/16.
//  Copyright © 2017 Orchard & Grove Inc. All rights reserved.
//

import Foundation
import SystemConfiguration
import IOKit

enum UtilityErrors: Error {
    case noSerialFound
    case noMACFound
}

/// A simple wrapper around NSTask
///
/// - Parameters:
///   - command: The `String` of the command to run. A full path to the binary is not required.
///              Arguments can be in the main string.
///   - arguments: An optional `Array` of `String` values that represent the arguments given to the command.
///                Defaults to 'nil'.
///   - waitForTermination: An optional `Bool` Should the the output be delayed until the task exits.
///                Deafults to 'true'.
/// - Returns: The combined result of standard output and standard error from the command.
public func cliTask(_ command: String, arguments: [String]? = nil, waitForTermination: Bool = true) -> String {

    var commandLaunchPath: String
    var commandPieces: [String]
    if arguments == nil {
        // turn the command into an array and get the first element as the launch path
        commandPieces = command.components(separatedBy: " ")

        // loop through the components and see if any end in \
        if command.contains("\\") {
            let newBits = commandPieces.map {$0.replacingOccurrences(of: "\\", with: " ")}
            commandPieces = newBits
        }
        commandLaunchPath = commandPieces.removeFirst()
    } else {
        commandLaunchPath = command
        commandPieces = arguments!
    }

    // make sure the launch path is the full path -- think we're going down a rabbit hole here

    if !commandLaunchPath.contains("/") {
        let realPath = which(commandLaunchPath)
        commandLaunchPath = realPath
    }

    // set up the NSTask instance and an NSPipe for the result

    let myTask = Process()
    let myPipe = Pipe()
    let myInputPipe = Pipe()
    let myErrorPipe = Pipe()

    // Setup and Launch!

    myTask.launchPath = commandLaunchPath
    myTask.arguments = commandPieces
    myTask.standardOutput = myPipe
    myTask.standardInput = myInputPipe
    myTask.standardError = myErrorPipe

    myTask.launch()
    if waitForTermination == true {
        myTask.waitUntilExit()
    }

    let data = myPipe.fileHandleForReading.readDataToEndOfFile()
    let error = myErrorPipe.fileHandleForReading.readDataToEndOfFile()
    let outputError = NSString(data: error, encoding: String.Encoding.utf8.rawValue)! as String
    let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String

    return output + outputError
}

/// A simple wrapper around NSTask that also doesn't wait for the `Task` termination signal. This function will be
/// removed soon.
///
/// - Parameter command: The `String` of the command to run. A full path to the binary is not required.
/// - Returns: The combined result of standard output and standard error from the command.
@available(*, deprecated)
public func cliTaskNoTerm(_ command: String) -> String {
    let returnValue = cliTask(command, arguments: nil, waitForTermination: true)
    return returnValue
}

/// Finds the serial number of the Mac.
///
/// - Returns: The serial number of the Mac as a `String`.
public func getSerial() throws -> String {
    let platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IOPlatformExpertDevice"))
    let serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert,
                                                                 kIOPlatformSerialNumberKey as CFString,
                                                                 kCFAllocatorDefault, 0)
    guard let serialNumber = serialNumberAsCFString!.takeUnretainedValue() as? String else {
        throw UtilityErrors.noSerialFound
    }
    return serialNumber
}

/// Finds the MAC address of the primary ethernet connection.
///
/// - Returns: First MAC address of Mac as a `String`.
public func getMAC() -> String {
    let myMACOutput = cliTask("/sbin/ifconfig -a").components(separatedBy: "\n")
    var myMac = ""

    for line in myMACOutput {
        if line.contains("ether") {
            myMac = line.replacingOccurrences(of: "ether", with: "").trimmingCharacters(in: CharacterSet.whitespaces)
            break
        }
    }
    return myMac
}

/// Private function to get the path to a binary if the full path isn't given
///
/// - Parameter command: A `String` of the UNIX command to find the qualified path to.
/// - Returns: A fully qualified path to any UNIX binary on the system from the which database.
private func which(_ command: String) -> String {
    let task = Process()
    task.launchPath = "/usr/bin/which"
    task.arguments = [command]

    let whichPipe = Pipe()
    task.standardOutput = whichPipe
    task.launch()

    let data = whichPipe.fileHandleForReading.readDataToEndOfFile()
    let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String

    if output == "" {
        NSLog("Binary doesn't exist")
    }
    return output.components(separatedBy: "\n").first!
}
